# © 2022 OAAFS
# License AGPLv3 (http://www.gnu.org/licenses/agpl-3.0-standalone.html)

from odoo_upgrade.init_environment import InitEnvironment

InitEnvironment().main()
