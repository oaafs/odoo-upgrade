# © 2022 OAAFS
# License AGPLv3 (http://www.gnu.org/licenses/agpl-3.0-standalone.html)

from .abstract_script import AbstractScript


class InitEnvironment(AbstractScript):
    def run(self, args):
        print("IN THE RUN !")
        return 5


def main():
    """Main CLI application."""
    return InitEnvironment().main()
