# © 2022 OAAFS
# License AGPLv3 (http://www.gnu.org/licenses/agpl-3.0-standalone.html)

import unittest

from odoo_upgrade.init_environment import InitEnvironment


class Test(unittest.TestCase):
    def test_init_environment(self):
        self.assertEqual(InitEnvironment().run([]), 5)
