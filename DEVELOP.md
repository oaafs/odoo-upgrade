### Install

```shell
# Install dependencies
pipx install coverage
apt-get install tox

# Pull Code
git clone https://gitlab.com/oaafs/odoo-upgrade
cd odoo-upgrade

# Create virtual env and activate it
virtualenv env --python=python3
. ./env/bin/activate

# Install dependencies
./env/bin/python -m pip install -r test_requirements.txt
./env/bin/python -m pip install -r requirements.txt

# run test via coverage
coverage run --source odoo_upgrade setup.py test

# run test via tox
tox

```
