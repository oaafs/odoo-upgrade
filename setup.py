# © 2022 OAAFS
# License AGPLv3 (http://www.gnu.org/licenses/agpl-3.0-standalone.html)

import setuptools

setuptools.setup(
    name="odoo-upgrade",
    use_scm_version=True,
    description="A library to upgrade your odoo instance"
    " from a version to another"
    " based on OCA/Openupgrade and Acsone/Gitagregate projects.",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "License :: OSI Approved ::"
        " GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Utilities",
        "Topic :: System :: Shells",
        "Framework :: Odoo",
    ],
    license="AGPLv3+",
    author="OAAFS",
    author_email="contact@oaafs.fr",
    url="https://gitlab.com/oaafs/odoo-upgrade",
    packages=[
        "odoo_upgrade",
    ],
    setup_requires=[
        "setuptools>=42",
        "setuptools_scm",
    ],
    install_requires=open("requirements.txt").read().splitlines(),
    entry_points=dict(
        console_scripts=[
            "odoo-upgrade-init=odoo_upgrade.init_environment:main",
        ]
    ),
    test_suite="tests",
)
