# © 2022 OAAFS
# License AGPLv3 (http://www.gnu.org/licenses/agpl-3.0-standalone.html)


import argparse
import logging
import time

import argcomplete
from colorama import Fore, Style

_LOG_LEVEL_STRINGS = ["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"]


logger = logging.getLogger(__name__)

LEVEL_COLORS = {
    "DEBUG": Fore.BLUE,  # Blue
    "INFO": Fore.GREEN,  # Green
    "WARNING": Fore.YELLOW,
    "ERROR": Fore.RED,
    "CRITICAL": Fore.RED,
}


def default_log_template(self, record):
    """Return the prefix for the log message. Template for Formatter.

    :param: record: :py:class:`logging.LogRecord` object. this is passed in
    from inside the :py:meth:`logging.Formatter.format` record.

    """

    reset = [Style.RESET_ALL]
    levelname = [
        LEVEL_COLORS.get(record.levelname),
        Style.BRIGHT,
        "(%(levelname)s)",
        Style.RESET_ALL,
        " ",
    ]
    asctime = [
        "[",
        Fore.BLACK,
        Style.DIM,
        Style.BRIGHT,
        "%(asctime)s",
        Fore.RESET,
        Style.RESET_ALL,
        "]",
    ]
    name = [
        " ",
        Fore.WHITE,
        Style.DIM,
        Style.BRIGHT,
        "%(name)s",
        Fore.RESET,
        Style.RESET_ALL,
        " ",
    ]
    threadName = [
        " ",
        Fore.BLUE,
        Style.DIM,
        Style.BRIGHT,
        "%(threadName)s ",
        Fore.RESET,
        Style.RESET_ALL,
        " ",
    ]

    tpl = "".join(reset + levelname + asctime + name + threadName + reset)

    return tpl


class LogFormatter(logging.Formatter):

    template = default_log_template

    def __init__(self, color=True, *args, **kwargs):
        logging.Formatter.__init__(self, *args, **kwargs)

    def format(self, record):
        try:
            record.message = record.getMessage()
        except Exception as e:
            record.message = "Bad message (%r): %r" % (e, record.__dict__)

        date_format = "%H:%M:%S"
        record.asctime = time.strftime(date_format, self.converter(record.created))

        prefix = self.template(record) % record.__dict__

        formatted = prefix + " " + record.message
        return formatted.replace("\n", "\n    ")


def _log_level_string_to_int(log_level_string):
    if log_level_string not in _LOG_LEVEL_STRINGS:
        message = "invalid choice: {0} (choose from {1})".format(
            log_level_string, _LOG_LEVEL_STRINGS
        )
        raise argparse.ArgumentTypeError(message)

    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed from our expected
    # values
    assert isinstance(log_level_int, int)

    return log_level_int


class AbstractScript:
    def setup_logger(self, log=None, level=logging.INFO):
        """Setup logging for CLI use.
        :param log: instance of logger
        :type log: :py:class:`Logger`
        """
        if not log:
            log = logging.getLogger()
        if not log.handlers:
            channel = logging.StreamHandler()
            channel.setFormatter(LogFormatter())
            log.setLevel(level)
            log.addHandler(channel)

    def get_parser(self):
        """Return :py:class:`argparse.ArgumentParser` instance for CLI."""

        main_parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter
        )

        main_parser.add_argument(
            "--log-level",
            default="INFO",
            dest="log_level",
            type=_log_level_string_to_int,
            nargs="?",
            help="Set the logging output level {0}".format(_LOG_LEVEL_STRINGS),
        )

        return main_parser

    def main(self):

        parser = self.get_parser()

        argcomplete.autocomplete(parser, always_complete_options=False)

        args = parser.parse_args()

        self.setup_logger(level=args.log_level)

        logger.info("bob")
        print("===============")
        print(args)
        print("===============")

        try:
            self.run(args)
        except KeyboardInterrupt:
            return 1
        return 5
